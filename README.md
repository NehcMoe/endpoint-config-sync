# Proxifier ppx 文件使用

## 1.Use method(使用方法)

1. **打开Proxifier软件，上方的菜单栏中File->Profile Auto Update**

![image-20220627171404471](https://gitlab.com/awesome_picture_group/git_picture/-/raw/main/pictures/2022/06/27_17_14_4_202206271714508.png)

2. **输入[配置文件URL](https://gitlab.com/NehcMoe/endpoint-config-sync/-/raw/main/Proxifier%20plus%20rule.ppx)**

![image-20220627171621485](https://gitlab.com/awesome_picture_group/git_picture/-/raw/main/pictures/2022/06/27_17_16_21_202206271716522.png)

3. **勾选Update profile on start from a web server(从网络服务器启动时更新配置文件)**

![image-20220627171707391](https://gitlab.com/awesome_picture_group/git_picture/-/raw/main/pictures/2022/06/27_17_17_7_202206271717429.png)

4. **将URL points to 选项更改为Single profile file**

![image-20220627171917719](https://gitlab.com/awesome_picture_group/git_picture/-/raw/main/pictures/2022/06/27_17_19_17_202206271719762.png)

5. **先执行一遍update now让它更新**

![image-20220627171955272](https://gitlab.com/awesome_picture_group/git_picture/-/raw/main/pictures/2022/06/27_17_19_55_202206271719307.png)

**当出现下面的提示框就表示已经更新成功了。**

**注意：如果更新不成功。请使用此配置文件更新地址，两个地址同步更新。**

![image-20220627172009871](https://gitlab.com/awesome_picture_group/git_picture/-/raw/main/pictures/2022/06/27_17_20_9_202206271720901.png)

**注意：当系统重启后，会自动向服务器获取最新配置文件。**
